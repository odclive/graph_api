# graph_api

GraphQL API for ODC Live.

## How to take care of ODC Live

All of the services are handled through Docker and more specifically,
`docker compose`. This makes it easy to update services or get rid of them
without interfering with the rest of them.

The `graph-api` repository can be seen as the central point of operations for
ODC Live as it provides the (small but nonetheless useful) database and API
keypoints.

Once cloned, make sure to run `git submodule init` and `git submodule update`.


## Backing up and restoring mails

The mailing solution we use is
[docker-mailserver](https://github.com/tomav/docker-mailserver). It is easy
to setup mailing account through a provided script and has shown portability
potential when backing up and restoring data from one host to another.

To do this very action, follow these steps:

1. Backup the data on host 1:

```sh
docker run --rm -ti \
  -v mail_maildata:/var/mail \
  -v mail_mailstate:/var/mail-state \
  -v /backup/mail:/backup \
  alpine:3.2 \
  tar czf /backup/mail-`date +%y%m%d-%H%M%S`.tgz /var/mail /var/mail-state

find /backup/mail -type f -mtime +30 -exec rm -f {} \;
```

2. `scp` the tgz from host 1 to host 2

3. `docker cp` the tgz to the running mail container

4. untar, cp the mail and mail-state directories to the correct /var one.

When changing host, don't forget to regenerate the certificates with letsencrypt

## Renewing/generating mail certificates with letsencrypt

1. Get in the correct directory `cd /home/ubuntu/docker/letsencrypt`

2. Update accordingly

```sh
docker run --rm -ti -v $PWD/log/:/var/log/letsencrypt/ \
  -v $PWD/etc/:/etc/letsencrypt/ \
  -p 80:80 deliverous/certbot certonly --standalone -d mail.odc.live
```

## Inspecting the database

1. Find the correct docker container with `docker ps`

2. `docker exec -it <container id> bash`

3. `psql -U postgres -d odclive`


## Getting rid of untagged (none) docker images

`docker rmi $(docker images | grep none | awk -F' ' '{print $3}')`

## launching local

this line should do the trick to launch it all

`docker compose --env-file .dev/dev.env --file docker-dev.yml build && docker compose --env-file .dev/dev.env --file docker-dev.yml up`


to launch db and api apart.

`docker compose --env-file .dev/dev.env --file docker-dev.yml up db -d`
`docker compose --env-file .dev/dev.env --file docker-dev.yml build api  && docker compose --env-file .dev/dev.env --file docker-dev.yml up api`

## launching prod

`docker compose --env-file .env build && docker compose --env-file .env up -d`