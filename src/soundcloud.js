const express = require("express");
const axios = require("axios");
const router = express.Router();
const client = `?client_id=${process.env.SND_ID}`;
const api = `https://api-v2.soundcloud.com/`;
const user = `${api}users/713729887/tracks${client}`;

router.use(express.json());

// expects a query key in order to fetch and send the right embedded html
router.get("/:offset", async (req, res) => {
  const link = `${user}&limit=12&offset=${req.params.offset}`;
  try {
    const sounds = await axios.get(link);
    res.send(sounds.data);
  } catch {
    res.status(404).json({
      error: "not found",
    });
  }
});

// getting tracks data
router.get("/tracks/:id", async (req, res) => {
  const link = `${api}tracks/${req.params.id}${client}`;
  try {
    const track = await axios.get(link);

    if (track.data.user.permalink != "zoneestradio") {
      res.status(404).json({
        error: "not found",
      });
    } else {
      res.send(track.data);
    }
  } catch {
    res.status(404).json({
      error: "not found",
    });
  }
});

// searching for a query with an offset
router.post("/search", async (req, res) => {
  let offset = req.body.offset ? req.body.offset : 0;

  tags = req.body.search
    .split(" ")
    .map((tag) => tag.trim())
    .filter((tag) => tag && tag != "" && tag.startsWith("#"));

  const scSearchUrl = "https://api-v2.soundcloud.com/search/sounds";
  const searchPrefix = "Z%E2%86%92EST%20Radio%20";
  const searchText = req.body.search.replace(/ #\s+/, "");
  const searchFilters = `filter.duration=epic&filter.genre=${tags[0]}`;

  const searchQuery = `q=${searchPrefix}${searchText}`;
  const link = `${scSearchUrl}?${searchQuery}&${searchFilters}&client_id=${
    process.env.SND_ID
  }&limit=20&offset=${offset}`;

  try {
    const track = await axios.get(link);
    res.send(track.data);
  } catch {
    res.status(403).json({
      error: "not found",
    });
  }
});

module.exports = router;
