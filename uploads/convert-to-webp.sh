QUALITY_LEVEL=50

# rename
for f in *.PNG ; do mv -i "${f}" "${f%.PNG}.png" ; done
for f in *.jpeg ; do mv -i "${f}" "${f%.jpeg}.jpg" ; done
for f in *.JPEG ; do mv -i "${f}" "${f%.JPEG}.jpg" ; done
for f in *.JPG ; do mv -i "${f}" "${f%.JPG}.jpg" ; done

# convert
for file in *.jpg; do
    cwebp -resize 800 0 -q $QUALITY_LEVEL "$file" -o "${file%.*}.webp";
done;

for file in *.png; do
    cwebp -resize 800 0 -q $QUALITY_LEVEL "$file" -o "${file%.*}.webp";
done;

export $(cat .env | xargs);
export PGPASSWORD=$DB_PASSWORD;
psql -h 127.0.0.0 -U postgres -f ./update-db-with-webp.sql --db odclive